//
//  HomeViewController.swift
//  Alert
//
//  Created by Akhil Waghmare on 10/17/15.
//  Copyright © 2015 AW Labs. All rights reserved.
//

import UIKit
import CoreData
import MessageUI
import CoreLocation

class HomeViewController: UIViewController, MFMessageComposeViewControllerDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var buttonTL: UIButton!
    @IBOutlet weak var buttonTR: UIButton!
    @IBOutlet weak var buttonBL: UIButton!
    @IBOutlet weak var buttonBR: UIButton!
    @IBOutlet weak var buttonEmergency: UIButton!
    @IBOutlet weak var buttonEdit: UIBarButtonItem!
    var buttons: [UIButton] = []
    
    var editMode: Bool = false
    var emergencyContact: NSManagedObject?
    var TLContact: NSManagedObject?
    var TRContact: NSManagedObject?
    var BLContact: NSManagedObject?
    var BRContact: NSManagedObject?
    
    let locationManager = CLLocationManager()
    var currentPlacemark: CLPlacemark?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        buttons += [buttonTL, buttonTR, buttonBL, buttonBR, buttonEmergency]
        
        for button in buttons {
            button.layer.cornerRadius = 0.5 * button.bounds.size.width;
        }
        
        do {
            try retrieveData()
        }
        catch {
            print(error)
        }
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func retrieveData() throws {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        // Fetch Emergency Contact
        var fetchRequest = NSFetchRequest(entityName: "EmergencyContact")
        // Execute the fetch request, and cast the results to an array
        if let fetchResults = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
            if fetchResults.count > 0 {
                emergencyContact = fetchResults[0]
            }
            else {
                // Create an Alert to tell user they haven't set emergency contact info
                let alert = UIAlertController(title: "Emergency Contacts Not Set",
                    message: "You should set emergency contacts",
                    preferredStyle: .Alert)
                let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alert.addAction(defaultAction)
                // Display the alert
                self.presentViewController(alert,
                    animated: true,
                    completion: nil)
            }
        }
        
        // Fetch Top Left Contact
        fetchRequest = NSFetchRequest(entityName: "TLContact")
        // Execute the fetch request, and cast the results to an array
        if let fetchResults = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
            if fetchResults.count > 0 {
                TLContact = fetchResults[0]
                if TLContact?.valueForKey("label") as! String != "" {
                    buttonTL.setTitle(TLContact?.valueForKey("label") as! String, forState: UIControlState.Normal)
                }
            }
        }
        
        // Fetch Top Right Contact
        fetchRequest = NSFetchRequest(entityName: "TRContact")
        // Execute the fetch request, and cast the results to an array
        if let fetchResults = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
            if fetchResults.count > 0 {
                TRContact = fetchResults[0]
                if TRContact?.valueForKey("label") as! String != "" {
                    buttonTR.setTitle(TRContact?.valueForKey("label") as! String, forState: UIControlState.Normal)
                }
            }
        }
        
        // Fetch Bottom Left Contact
        fetchRequest = NSFetchRequest(entityName: "BLContact")
        // Execute the fetch request, and cast the results to an array
        if let fetchResults = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
            if fetchResults.count > 0 {
                BLContact = fetchResults[0]
                if BLContact?.valueForKey("label") as! String != "" {
                    buttonBL.setTitle(BLContact?.valueForKey("label") as! String, forState: UIControlState.Normal)
                }
            }
        }
        
        // Fetch Bottom Right Contact
        fetchRequest = NSFetchRequest(entityName: "BRContact")
        // Execute the fetch request, and cast the results to an array
        if let fetchResults = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
            if fetchResults.count > 0 {
                BRContact = fetchResults[0]
                if BRContact?.valueForKey("label") as! String != "" {
                    buttonBR.setTitle(BRContact?.valueForKey("label") as! String, forState: UIControlState.Normal)
                }
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editPressed(sender: AnyObject) {
        if editMode {
            editMode = false
            buttonEdit.title = "Edit"
            do {
                try retrieveData()
            }
            catch {
                print(error)
            }
        }
        else {
            editMode = true
            buttonEdit.title = "Done"
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "editAction"{
            let buttonSender = sender! as! UIButton
            if let index = buttons.indexOf(buttonSender){
                let actionViewController = segue.destinationViewController as! ActionViewController
                actionViewController.actionDisplay = index
            }
        }
    }
    
    func retrieveContacts(contact:NSManagedObject?) -> [String] {
        var recipients: [String] = []
        if let number1 = contact?.valueForKey("number1") {
            if number1 as! String != "" {
                recipients += [number1 as! String]
            }
        }
        if let number2 = contact?.valueForKey("number2") {
            if number2 as! String != "" {
                recipients += [number2 as! String]
            }
        }
        if let number3 = contact?.valueForKey("number3") {
            if number3 as! String != "" {
                recipients += [number3 as! String]
            }
        }
        if let number4 = contact?.valueForKey("number4") {
            if number4 as! String != "" {
                recipients += [number4 as! String]
            }
        }
        if let number5 = contact?.valueForKey("number5") {
            if number5 as! String != "" {
                recipients += [number5 as! String]
            }
        }
        if let number6 = contact?.valueForKey("number6") {
            if number6 as! String != "" {
                recipients += [number6 as! String]
            }
        }
        return recipients
    }
    
    @IBAction func buttonEmergencyPressed(sender: AnyObject) {
        if editMode {
            performSegueWithIdentifier("editAction", sender: sender)
        }
        else {
            var recipients = retrieveContacts(emergencyContact)
            if recipients.count > 0 {
                sendSMSLocation(recipients)
            }
        }
    }
    
    @IBAction func buttonTLPressed(sender: AnyObject) {
        if editMode {
            performSegueWithIdentifier("editAction", sender: sender)
        }
        else {
            let actionType = TLContact?.valueForKey("actionType")
            if actionType as! String == "0" {
                callPhone(TLContact?.valueForKey("number1") as! String)
            }
            else if actionType as! String == "1" {
                sendSMS(retrieveContacts(TLContact), message: TLContact?.valueForKey("message") as! String)
            }
        }
    }
    
    @IBAction func buttonTRPressed(sender: AnyObject) {
        if editMode {
            performSegueWithIdentifier("editAction", sender: sender)
        }
        else {
            let actionType = TRContact?.valueForKey("actionType")
            if actionType as! String == "0" {
                callPhone(TRContact?.valueForKey("number1") as! String)
            }
            else if actionType as! String == "1" {
                sendSMS(retrieveContacts(TRContact), message: TRContact?.valueForKey("message") as! String)
            }
        }
    }
    @IBAction func buttonBLPressed(sender: AnyObject) {
        if editMode {
            performSegueWithIdentifier("editAction", sender: sender)
        }
        else {
            let actionType = BLContact?.valueForKey("actionType")
            if actionType as! String == "0" {
                callPhone(BLContact?.valueForKey("number1") as! String)
            }
            else if actionType as! String == "1" {
                sendSMS(retrieveContacts(BLContact), message: BLContact?.valueForKey("message") as! String)
            }
        }
    }

    @IBAction func buttonBRPressed(sender: AnyObject) {
        if editMode {
            performSegueWithIdentifier("editAction", sender: sender)
        }
        else {
            let actionType = BRContact?.valueForKey("actionType")
            if actionType as! String == "0" {
                callPhone(BRContact?.valueForKey("number1") as! String)
            }
            else if actionType as! String == "1" {
                sendSMS(retrieveContacts(BRContact), message: BRContact?.valueForKey("message") as! String)
            }
        }
    }
    
    // MARK: Functionality Code
    
    func sendSMS(recipients:[String], message:String) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = message
            controller.recipients = recipients
            controller.messageComposeDelegate = self
            self.presentViewController(controller, animated: true, completion: nil)
        }
    }
    
    func callPhone(recipient:String) {
        let phone = "tel://" + recipient
        let url: NSURL = NSURL(string: phone)!
        UIApplication.sharedApplication().openURL(url)
    }
    
    func sendSMSLocation(recipients:[String]) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            if let currentLocation = currentPlacemark {
                controller.body = "Emergency. Need help. \(currentLocation.subThoroughfare!) \(currentLocation.thoroughfare!), \(currentLocation.locality!), \(currentLocation.administrativeArea!) \(currentLocation.postalCode!)"
                controller.recipients = recipients
                controller.messageComposeDelegate = self
                self.presentViewController(controller, animated: true, completion: nil)
            }
        }
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: { (placemarks, error) -> Void in
            
            if(error != nil){
                print("Error: \(error!.localizedDescription)")
                return
            }
            
            if(placemarks!.count > 0){
                let pm = placemarks![0] as CLPlacemark
                self.currentPlacemark = pm
            } else {
                print("Error with data")
            }
            
        })
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location " + error.localizedDescription)
    }
}

