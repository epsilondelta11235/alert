//
//  ActionViewController.swift
//  Alert
//
//  Created by Akhil Waghmare on 10/17/15.
//  Copyright © 2015 AW Labs. All rights reserved.
//

import UIKit
import CoreData

class ActionViewController: UIViewController {
    
    var actionDisplay: Int?
    var data: NSManagedObject?
    var actionTitles = ["TLContact", "TRContact", "BLContact", "BRContact", "EmergencyContact"]
    
    @IBOutlet weak var actionName: UITextField!
    @IBOutlet weak var actionType: UISegmentedControl!
    @IBOutlet weak var contact1: UITextField!
    @IBOutlet weak var contact2: UITextField!
    @IBOutlet weak var contact3: UITextField!
    @IBOutlet weak var contact4: UITextField!
    @IBOutlet weak var contact5: UITextField!
    @IBOutlet weak var contact6: UITextField!
    @IBOutlet weak var messageText: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        do {
            try retrieveData()
        }
        catch {
            print(error)
        }
        populateView()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        if let dataUsed = data {
            data!.setValue(contact1.text, forKey: "number1")
            data!.setValue(contact2.text, forKey: "number2")
            data!.setValue(contact3.text, forKey: "number3")
            data!.setValue(contact4.text, forKey: "number4")
            data!.setValue(contact5.text, forKey: "number5")
            data!.setValue(contact6.text, forKey: "number6")
            if actionDisplay != 4 {
                data!.setValue(messageText.text, forKey: "message")
                data!.setValue(actionName.text, forKey: "label")
                data!.setValue("\(actionType.selectedSegmentIndex)", forKey: "actionType")
            }
            do {
                try managedContext.save()
            }
            catch {
                print(error)
            }
        }
        else {
            var newData = NSEntityDescription.insertNewObjectForEntityForName(actionTitles[actionDisplay!], inManagedObjectContext: managedContext) as! NSManagedObject
            newData.setValue(contact1.text, forKey: "number1")
            newData.setValue(contact2.text, forKey: "number2")
            newData.setValue(contact3.text, forKey: "number3")
            newData.setValue(contact4.text, forKey: "number4")
            newData.setValue(contact5.text, forKey: "number5")
            newData.setValue(contact6.text, forKey: "number6")
            if actionDisplay != 4 {
                newData.setValue(messageText.text, forKey: "message")
                newData.setValue(actionName.text, forKey: "label")
                newData.setValue("\(actionType.selectedSegmentIndex)", forKey: "actionType")
            }
        }
    }
    
    func retrieveData() throws {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        // Fetch Emergency Contact
        var fetchRequest = NSFetchRequest(entityName: actionTitles[actionDisplay!])
        // Execute the fetch request, and cast the results to an array
        if let fetchResults = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
            if fetchResults.count > 0 {
                data = fetchResults[0]
            }
        }
    }
    
    func populateView() {
        if let number1 = data?.valueForKey("number1") {
            contact1.text = number1 as! String
        }
        if let number2 = data?.valueForKey("number2") {
            contact2.text = number2 as! String
        }
        if let number3 = data?.valueForKey("number3") {
            contact3.text = number3 as! String
        }
        if let number4 = data?.valueForKey("number4") {
            contact4.text = number4 as! String
        }
        if let number5 = data?.valueForKey("number5") {
            contact5.text = number5 as! String
        }
        if let number6 = data?.valueForKey("number6") {
            contact6.text = number6 as! String
        }
        
        if actionDisplay != 4 {
            if let message = data?.valueForKey("message") {
                messageText.text = message as! String
            }
            if let label = data?.valueForKey("label") {
                actionName.text = label as! String
            }
            if let storedActionType = data?.valueForKey("actionType") {
                if storedActionType as! String == "0" {
                    actionType.selectedSegmentIndex = 0
                }
                else if storedActionType as! String == "1" {
                    actionType.selectedSegmentIndex = 1
                }
            }
        }
        else {
           actionType.selectedSegmentIndex = 1
           actionType.enabled = false
           actionName.text = "Emergency"
           actionName.enabled = false
           messageText.text = "Emergency. Need help. {Current location}"
        }
        toggleViews(actionType.selectedSegmentIndex)
        if actionDisplay == 4 {
            messageText.editable = false
            messageText.backgroundColor = UIColor.grayColor()
            actionName.backgroundColor = UIColor.grayColor()
        }
    }
    
    func toggleViews(state: Int) {
        if state == 0 {
            contact2.text = ""
            contact2.backgroundColor = UIColor.grayColor()
            contact3.text = ""
            contact3.backgroundColor = UIColor.grayColor()
            contact4.text = ""
            contact4.backgroundColor = UIColor.grayColor()
            contact5.text = ""
            contact5.backgroundColor = UIColor.grayColor()
            contact6.text = ""
            contact6.backgroundColor = UIColor.grayColor()
            messageText.text = ""
            messageText.backgroundColor = UIColor.grayColor()
            
            contact2.enabled = false
            contact3.enabled = false
            contact4.enabled = false
            contact5.enabled = false
            contact6.enabled = false
            messageText.editable = false
            
            actionName.backgroundColor = UIColor.whiteColor()
        }
        else if state == 1 {
            contact2.enabled = true
            contact2.backgroundColor = UIColor.whiteColor()
            contact3.enabled = true
            contact3.backgroundColor = UIColor.whiteColor()
            contact4.enabled = true
            contact4.backgroundColor = UIColor.whiteColor()
            contact5.enabled = true
            contact5.backgroundColor = UIColor.whiteColor()
            contact6.enabled = true
            contact6.backgroundColor = UIColor.whiteColor()
            messageText.editable = true
            messageText.backgroundColor = UIColor.whiteColor()
            
            actionName.backgroundColor = UIColor.whiteColor()
        }
    }
    
    @IBAction func segmentSwitch(sender: AnyObject) {
        var selectedSegment = actionType.selectedSegmentIndex
        toggleViews(selectedSegment)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

